oc login -u system:admin
oc create -f C://users/naveengu/redhat-openjdk18-openshift.json -n openshift

oc policy add-role-to-user cluster-developer developer
oc policy add-role-to-user developer developer

oc policy add-role-to-user developer system:serviceaccount -n openshift
oc policy add-role-to-user edit system:serviceaccount -n openshift

oc login -u developer -p dev
oc new-project jenkins --display-name=JENKINS
oc new-app --template=jenkins-persistent -p MEMORY_LIMIT=1Gi -l app=jenkins

oc new-project dev --display-name=DEV
oc new-project test --display-name=TEST
oc new-project prod --display-name=PROD

# Grant edit access to developer in dev project
oc adm policy add-role-to-user edit developer -n dev

# Grant view access to developer in test project
oc adm policy add-role-to-user view developer -n test

# Grant view access to developer in prod project
oc adm policy add-role-to-user view developer -n prod

# Grant view access to developer in jenkins project
oc adm policy add-role-to-user edit developer -n jenkins

# Grant edit access to jenkins service account
oc policy add-role-to-user edit system:serviceaccount:jenkins:jenkins -n dev
oc policy add-role-to-user edit system:serviceaccount:jenkins:jenkins -n test
oc policy add-role-to-user edit system:serviceaccount:jenkins:jenkins -n prod

# Allow prod service account the ability to pull images from dev
oc policy add-role-to-group system:image-puller system:serviceaccounts:test -n dev
oc policy add-role-to-group system:image-puller system:serviceaccounts:prod -n dev



oc project dev

# Creates a binary build (the build is not started immediately)
oc new-build --binary=true --name="app" wildfly --allow-missing-imagestream-tags

#oc set label app=app

# Creates the application
oc new-app dev/app:DevCandidate-1.0.0 --name="app" --allow-missing-imagestream-tags=true

# Removes the triggers
oc set triggers dc/app --remove-all

oc expose dc/app --port 8080
oc expose svc/app

oc project test

oc new-app dev/app:TestCandidate-1.0.0 --name="app" --allow-missing-imagestream-tags=true

# Removes the triggers
oc set triggers dc/app --remove-all

oc expose dc/app --port 8080
oc expose svc/app

#
# Production applications
#

# Blue/Green

oc project prod

# Creates the blue and green applications (observe that in prod is not a BuildConfig object created)
oc new-app dev/app:ProdReady-1.0.0 --name="app-green" --allow-missing-imagestream-tags=true
oc new-app dev/app:ProdReady-1.0.0 --name="app-blue" --allow-missing-imagestream-tags=true

# Removes the triggers
oc set triggers dc/app-green --remove-all
oc set triggers dc/app-blue --remove-all

oc expose dc/app-blue --port 8080
oc expose dc/app-green --port 8080

oc expose svc/app-green --name blue-green

oc project jenkins

oc new-app https://dgauda@bitbucket.org/dgauda/parking-simulator-poc#master --context-dir=cicd/pipelines/ci --name ci-pipeline

oc new-app https://dgauda@bitbucket.org/dgauda/parking-simulator-poc#master --context-dir=cicd/pipelines/cd --name cd-pipeline