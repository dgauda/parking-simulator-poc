package com.cap.simulatorservices.parkingmanagement.dataaccess.api;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.cap.simulatorservices.general.dataaccess.api.ApplicationPersistenceEntity;
import com.cap.simulatorservices.parkingmanagement.common.api.ParkingLot;

/**
 * @author dgauda
 */
@Entity
@Table(name = "ParkingLot")
public class ParkingLotEntity extends ApplicationPersistenceEntity implements ParkingLot {

  private String address;

  private Double latitude;

  private Double longitude;

  private Long totalNoOfSlots;

  private Long availableNoOfSlots;

  private String operatingcompany;

  private Set<ParkingSlotEntity> parkingSlots;

  private static final long serialVersionUID = 1L;

  @OneToMany(mappedBy = "parkingLot", fetch = FetchType.LAZY)
  public Set<ParkingSlotEntity> getParkingSlots() {

    return this.parkingSlots;
  }

  public void setParkingSlots(Set<ParkingSlotEntity> parkingSlots) {

    this.parkingSlots = parkingSlots;
  }

  /**
   * @return address
   */
  public String getAddress() {

    return this.address;
  }

  /**
   * @param address new value of {@link #getaddress}.
   */
  public void setAddress(String address) {

    this.address = address;
  }

  /**
   * @return latitude
   */
  public Double getLatitude() {

    return this.latitude;
  }

  /**
   * @param latitude new value of {@link #getlatitude}.
   */
  public void setLatitude(Double latitude) {

    this.latitude = latitude;
  }

  /**
   * @return longitude
   */
  public Double getLongitude() {

    return this.longitude;
  }

  /**
   * @param longitude new value of {@link #getlongitude}.
   */
  public void setLongitude(Double longitude) {

    this.longitude = longitude;
  }

  /**
   * @return totalNoOfSlots
   */
  public Long getTotalNoOfSlots() {

    return this.totalNoOfSlots;
  }

  /**
   * @param totalNoOfSlots new value of {@link #gettotalNoOfSlots}.
   */
  public void setTotalNoOfSlots(Long totalNoOfSlots) {

    this.totalNoOfSlots = totalNoOfSlots;
  }

  /**
   * @return availableNoOfSlots
   */
  public Long getAvailableNoOfSlots() {

    return this.availableNoOfSlots;
  }

  /**
   * @param availableNoOfSlots new value of {@link #getavailableNoOfSlots}.
   */
  public void setAvailableNoOfSlots(Long availableNoOfSlots) {

    this.availableNoOfSlots = availableNoOfSlots;
  }

  /**
   * @return operatingcompany
   */
  public String getOperatingcompany() {

    return this.operatingcompany;
  }

  /**
   * @param operatingcompany new value of {@link #getoperatingcompany}.
   */
  public void setOperatingcompany(String operatingcompany) {

    this.operatingcompany = operatingcompany;
  }

}
