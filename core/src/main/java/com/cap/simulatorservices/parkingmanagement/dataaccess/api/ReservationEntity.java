package com.cap.simulatorservices.parkingmanagement.dataaccess.api;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cap.simulatorservices.general.dataaccess.api.ApplicationPersistenceEntity;
import com.cap.simulatorservices.parkingmanagement.common.api.Reservation;

/**
 * @author dgauda
 */
@Entity
@Table(name = "Reservation")
public class ReservationEntity extends ApplicationPersistenceEntity implements Reservation {

  private Long itineraryId;

  @Temporal(TemporalType.TIMESTAMP)
  private Timestamp startTimestamp;

  @Temporal(TemporalType.TIMESTAMP)
  private Timestamp endTimestamp;

  private ParkingLotEntity parkingLot;

  private ParkingSlotEntity parkingSlot;

  private static final long serialVersionUID = 1L;

  /**
   * @return itineraryId
   */
  public Long getItineraryId() {

    return this.itineraryId;
  }

  /**
   * @param itineraryId new value of {@link #getitineraryId}.
   */
  public void setItineraryId(Long itineraryId) {

    this.itineraryId = itineraryId;
  }

  /**
   * @return startTimestamp
   */
  public Timestamp getStartTimestamp() {

    return this.startTimestamp;
  }

  /**
   * @param startTimestamp new value of {@link #getstartTimestamp}.
   */
  public void setStartTimestamp(Timestamp startTimestamp) {

    this.startTimestamp = startTimestamp;
  }

  /**
   * @return endTimestamp
   */
  public Timestamp getEndTimestamp() {

    return this.endTimestamp;
  }

  /**
   * @param endTimestamp new value of {@link #getendTimestamp}.
   */
  public void setEndTimestamp(Timestamp endTimestamp) {

    this.endTimestamp = endTimestamp;
  }

  /**
   * @return parkingLotDetails
   */
  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "idParkingLot")
  public ParkingLotEntity getParkingLot() {

    return this.parkingLot;
  }

  /**
   * @param parkingLotDetails new value of {@link #getparkingLotDetails}.
   */
  public void setParkingLot(ParkingLotEntity parkingLot) {

    this.parkingLot = parkingLot;
  }

  /**
   * @return parkingSlotDetails
   */
  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "idParkingSlot")
  public ParkingSlotEntity getParkingSlot() {

    return this.parkingSlot;
  }

  /**
   * @param parkingSlotDetails new value of {@link #getparkingSlotDetails}.
   */
  public void setParkingSlot(ParkingSlotEntity parkingSlot) {

    this.parkingSlot = parkingSlot;
  }

  @Override
  @Transient
  public Long getParkingLotId() {

    if (this.parkingLot == null) {
      return null;
    }
    return this.parkingLot.getId();
  }

  @Override
  public void setParkingLotId(Long parkingLotId) {

    if (parkingLotId == null) {
      this.parkingLot = null;
    } else {
      ParkingLotEntity parkingLotEntity = new ParkingLotEntity();
      parkingLotEntity.setId(parkingLotId);
      this.parkingLot = parkingLotEntity;
    }
  }

  @Override
  @Transient
  public Long getParkingSlotId() {

    if (this.parkingSlot == null) {
      return null;
    }
    return this.parkingSlot.getId();
  }

  @Override
  public void setParkingSlotId(Long parkingSlotId) {

    if (parkingSlotId == null) {
      this.parkingLot = null;
    } else {
      ParkingSlotEntity parkingSlotEntity = new ParkingSlotEntity();
      parkingSlotEntity.setId(parkingSlotId);
      this.parkingSlot = parkingSlotEntity;
    }
  }

}
