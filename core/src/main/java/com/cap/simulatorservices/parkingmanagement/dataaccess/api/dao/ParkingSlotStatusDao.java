package com.cap.simulatorservices.parkingmanagement.dataaccess.api.dao;

import com.cap.simulatorservices.general.dataaccess.api.dao.ApplicationDao;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingSlotStatusEntity;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * Data access interface for ParkingSlotStatus entities
 */
public interface ParkingSlotStatusDao extends ApplicationDao<ParkingSlotStatusEntity> {

  /**
   * Finds the {@link ParkingSlotStatusEntity parkingslotstatuss} matching the given
   * {@link ParkingSlotStatusSearchCriteriaTo}.
   *
   * @param criteria is the {@link ParkingSlotStatusSearchCriteriaTo}.
   * @return the {@link PaginatedListTo} with the matching {@link ParkingSlotStatusEntity} objects.
   */
  PaginatedListTo<ParkingSlotStatusEntity> findParkingSlotStatuss(ParkingSlotStatusSearchCriteriaTo criteria);
}
