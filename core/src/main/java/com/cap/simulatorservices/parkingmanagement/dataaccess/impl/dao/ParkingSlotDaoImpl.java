package com.cap.simulatorservices.parkingmanagement.dataaccess.impl.dao;

import java.util.List;

import javax.inject.Named;

import com.cap.simulatorservices.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingSlotEntity;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.dao.ParkingSlotDao;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotSearchCriteriaTo;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link ParkingSlotDao}.
 */
@Named
public class ParkingSlotDaoImpl extends ApplicationDaoImpl<ParkingSlotEntity> implements ParkingSlotDao {

  /**
   * The constructor.
   */
  public ParkingSlotDaoImpl() {

    super();
  }

  @Override
  public Class<ParkingSlotEntity> getEntityClass() {

    return ParkingSlotEntity.class;
  }

  @Override
  public PaginatedListTo<ParkingSlotEntity> findParkingSlots(ParkingSlotSearchCriteriaTo criteria) {

    ParkingSlotEntity parkingslot = Alias.alias(ParkingSlotEntity.class);
    EntityPathBase<ParkingSlotEntity> alias = Alias.$(parkingslot);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    Long slotNumber = criteria.getSlotNumber();
    if (slotNumber != null) {
      query.where(Alias.$(parkingslot.getSlotNumber()).eq(slotNumber));
    }
    Long parkingLotDetails = criteria.getParkingLotDetailsId();
    if (parkingLotDetails != null) {
      if (parkingslot.getParkingLot() != null) {
        query.where(Alias.$(parkingslot.getParkingLot().getId()).eq(parkingLotDetails));
      }
    }
    addOrderBy(query, alias, parkingslot, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<ParkingSlotEntity> alias, ParkingSlotEntity parkingslot,
      List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "slotNumber":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(parkingslot.getSlotNumber()).asc());
            } else {
              query.orderBy(Alias.$(parkingslot.getSlotNumber()).desc());
            }
            break;
          case "parkingLotDetails":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(parkingslot.getParkingLot().getId()).asc());
            } else {
              query.orderBy(Alias.$(parkingslot.getParkingLot().getId()).desc());
            }
            break;
        }
      }
    }
  }

}