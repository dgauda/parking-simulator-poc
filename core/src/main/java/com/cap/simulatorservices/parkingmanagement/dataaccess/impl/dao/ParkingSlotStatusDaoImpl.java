package com.cap.simulatorservices.parkingmanagement.dataaccess.impl.dao;

import java.util.List;

import javax.inject.Named;

import com.cap.simulatorservices.general.dataaccess.base.dao.ApplicationDaoImpl;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingSlotStatusEntity;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.dao.ParkingSlotStatusDao;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusSearchCriteriaTo;
import com.mysema.query.alias.Alias;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.path.EntityPathBase;

import io.oasp.module.jpa.common.api.to.OrderByTo;
import io.oasp.module.jpa.common.api.to.OrderDirection;
import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * This is the implementation of {@link ParkingSlotStatusDao}.
 */
@Named
public class ParkingSlotStatusDaoImpl extends ApplicationDaoImpl<ParkingSlotStatusEntity>
    implements ParkingSlotStatusDao {

  /**
   * The constructor.
   */
  public ParkingSlotStatusDaoImpl() {

    super();
  }

  @Override
  public Class<ParkingSlotStatusEntity> getEntityClass() {

    return ParkingSlotStatusEntity.class;
  }

  @Override
  public PaginatedListTo<ParkingSlotStatusEntity> findParkingSlotStatuss(ParkingSlotStatusSearchCriteriaTo criteria) {

    ParkingSlotStatusEntity parkingslotstatus = Alias.alias(ParkingSlotStatusEntity.class);
    EntityPathBase<ParkingSlotStatusEntity> alias = Alias.$(parkingslotstatus);
    JPAQuery query = new JPAQuery(getEntityManager()).from(alias);

    String name = criteria.getName();
    if (name != null) {
      query.where(Alias.$(parkingslotstatus.getName()).eq(name));
    }
    String desc = criteria.getDesc();
    if (desc != null) {
      query.where(Alias.$(parkingslotstatus.getDesc()).eq(desc));
    }
    addOrderBy(query, alias, parkingslotstatus, criteria.getSort());

    return findPaginated(criteria, query, alias);
  }

  private void addOrderBy(JPAQuery query, EntityPathBase<ParkingSlotStatusEntity> alias,
      ParkingSlotStatusEntity parkingslotstatus, List<OrderByTo> sort) {

    if (sort != null && !sort.isEmpty()) {
      for (OrderByTo orderEntry : sort) {
        switch (orderEntry.getName()) {
          case "name":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(parkingslotstatus.getName()).asc());
            } else {
              query.orderBy(Alias.$(parkingslotstatus.getName()).desc());
            }
            break;
          case "desc":
            if (OrderDirection.ASC.equals(orderEntry.getDirection())) {
              query.orderBy(Alias.$(parkingslotstatus.getDesc()).asc());
            } else {
              query.orderBy(Alias.$(parkingslotstatus.getDesc()).desc());
            }
            break;
        }
      }
    }
  }

}