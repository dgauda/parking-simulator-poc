package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import com.cap.simulatorservices.general.common.api.to.AbstractCto;

/**
 * Composite transport object of Reservation
 */
public class ReservationCto extends AbstractCto {

  private static final long serialVersionUID = 1L;

  private ReservationEto reservation;

  private ParkingLotEto parkingLotDetails;

  private ParkingSlotEto parkingSlotDetails;

  public ReservationEto getReservation() {

    return this.reservation;
  }

  public void setReservation(ReservationEto reservation) {

    this.reservation = reservation;
  }

  public ParkingLotEto getParkingLotDetails() {

    return this.parkingLotDetails;
  }

  public void setParkingLotDetails(ParkingLotEto parkingLotDetails) {

    this.parkingLotDetails = parkingLotDetails;
  }

  public ParkingSlotEto getParkingSlotDetails() {

    return this.parkingSlotDetails;
  }

  public void setParkingSlotDetails(ParkingSlotEto parkingSlotDetails) {

    this.parkingSlotDetails = parkingSlotDetails;
  }

}
