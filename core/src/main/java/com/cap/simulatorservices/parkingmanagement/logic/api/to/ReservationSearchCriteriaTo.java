package com.cap.simulatorservices.parkingmanagement.logic.api.to;

import java.sql.Timestamp;

import io.oasp.module.jpa.common.api.to.SearchCriteriaTo;

/**
 * This is the {@link SearchCriteriaTo search criteria} {@link net.sf.mmm.util.transferobject.api.TransferObject TO}
 * used to find {@link com.cap.simulatorservices.parkingmanagement.common.api.Reservation}s.
 *
 */
public class ReservationSearchCriteriaTo extends SearchCriteriaTo {

  private static final long serialVersionUID = 1L;

  private Long itineraryId;

  private Timestamp startTimestamp;

  private Timestamp endTimestamp;

  private Long parkingLotId;

  /**
   * The constructor.
   */
  public ReservationSearchCriteriaTo() {

    super();
  }

  public Long getItineraryId() {

    return this.itineraryId;
  }

  public void setItineraryId(Long itineraryId) {

    this.itineraryId = itineraryId;
  }

  public Timestamp getStartTimestamp() {

    return this.startTimestamp;
  }

  public void setStartTimestamp(Timestamp startTimestamp) {

    this.startTimestamp = startTimestamp;
  }

  public Timestamp getEndTimestamp() {

    return this.endTimestamp;
  }

  public void setEndTimestamp(Timestamp endTimestamp) {

    this.endTimestamp = endTimestamp;
  }

  public Long getParkingLotId() {

    return this.parkingLotId;
  }

  public void setParkingLotId(Long parkingLotId) {

    this.parkingLotId = parkingLotId;
  }

}
