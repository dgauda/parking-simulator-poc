package com.cap.simulatorservices.parkingmanagement.logic.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.cap.simulatorservices.general.logic.base.AbstractComponentFacade;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingLotEntity;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingSlotEntity;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ParkingSlotStatusEntity;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.ReservationEntity;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.dao.ParkingLotDao;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.dao.ParkingSlotDao;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.dao.ParkingSlotStatusDao;
import com.cap.simulatorservices.parkingmanagement.dataaccess.api.dao.ReservationDao;
import com.cap.simulatorservices.parkingmanagement.logic.api.Parkingmanagement;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;
import io.oasp.module.jpa.common.api.to.PaginationResultTo;

/**
 * Implementation of component interface of parkingmanagement
 */
@Named
@Transactional
public class ParkingmanagementImpl extends AbstractComponentFacade implements Parkingmanagement {

  /**
   * Logger instance.
   */
  private static final Logger LOG = LoggerFactory.getLogger(ParkingmanagementImpl.class);

  /**
   * @see #getParkingLotDao()
   */
  @Inject
  private ParkingLotDao parkingLotDao;

  /**
   * @see #getParkingSlotDao()
   */
  @Inject
  private ParkingSlotDao parkingSlotDao;

  /**
   * @see #getReservationDao()
   */
  @Inject
  private ReservationDao reservationDao;

  /**
   * @see #getParkingSlotStatusDao()
   */
  @Inject
  private ParkingSlotStatusDao parkingSlotStatusDao;

  /**
   * The constructor.
   */
  public ParkingmanagementImpl() {

    super();
  }

  @Override
  public ParkingLotEto findParkingLot(Long id) {

    LOG.debug("Get ParkingLot with id {} from database.", id);
    return getBeanMapper().map(getParkingLotDao().findOne(id), ParkingLotEto.class);
  }

  @Override
  public PaginatedListTo<ParkingLotEto> findParkingLotEtos(ParkingLotSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ParkingLotEntity> parkinglots = getParkingLotDao().findParkingLots(criteria);
    return mapPaginatedEntityList(parkinglots, ParkingLotEto.class);
  }

  @Override
  public boolean deleteParkingLot(Long parkingLotId) {

    ParkingLotEntity parkingLot = getParkingLotDao().find(parkingLotId);
    getParkingLotDao().delete(parkingLot);
    LOG.debug("The parkingLot with id '{}' has been deleted.", parkingLotId);
    return true;
  }

  @Override
  public ParkingLotEto saveParkingLot(ParkingLotEto parkingLot) {

    Objects.requireNonNull(parkingLot, "parkingLot");
    ParkingLotEntity parkingLotEntity = getBeanMapper().map(parkingLot, ParkingLotEntity.class);

    // initialize, validate parkingLotEntity here if necessary
    ParkingLotEntity resultEntity = getParkingLotDao().save(parkingLotEntity);
    LOG.debug("ParkingLot with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, ParkingLotEto.class);
  }

  /**
   * Returns the field 'parkingLotDao'.
   *
   * @return the {@link ParkingLotDao} instance.
   */
  public ParkingLotDao getParkingLotDao() {

    return this.parkingLotDao;
  }

  @Override
  public ParkingLotCto findParkingLotCto(Long id) {

    LOG.debug("Get ParkingLotCto with id {} from database.", id);
    ParkingLotEntity entity = getParkingLotDao().findOne(id);
    ParkingLotCto cto = new ParkingLotCto();
    cto.setParkingLot(getBeanMapper().map(entity, ParkingLotEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<ParkingLotCto> findParkingLotCtos(ParkingLotSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ParkingLotEntity> parkinglots = getParkingLotDao().findParkingLots(criteria);
    List<ParkingLotCto> ctos = new ArrayList<>();
    for (ParkingLotEntity entity : parkinglots.getResult()) {
      ParkingLotCto cto = new ParkingLotCto();
      cto.setParkingLot(getBeanMapper().map(entity, ParkingLotEto.class));
      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<ParkingLotCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

  @Override
  public ParkingSlotEto findParkingSlot(Long id) {

    LOG.debug("Get ParkingSlot with id {} from database.", id);
    return getBeanMapper().map(getParkingSlotDao().findOne(id), ParkingSlotEto.class);
  }

  @Override
  public PaginatedListTo<ParkingSlotEto> findParkingSlotEtos(ParkingSlotSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ParkingSlotEntity> parkingslots = getParkingSlotDao().findParkingSlots(criteria);
    return mapPaginatedEntityList(parkingslots, ParkingSlotEto.class);
  }

  @Override
  public boolean deleteParkingSlot(Long parkingSlotId) {

    ParkingSlotEntity parkingSlot = getParkingSlotDao().find(parkingSlotId);
    getParkingSlotDao().delete(parkingSlot);
    LOG.debug("The parkingSlot with id '{}' has been deleted.", parkingSlotId);
    return true;
  }

  @Override
  public ParkingSlotEto saveParkingSlot(ParkingSlotEto parkingSlot) {

    Objects.requireNonNull(parkingSlot, "parkingSlot");
    // ParkingSlotEntity parkingSlotEntity = getBeanMapper().map(parkingSlot, ParkingSlotEntity.class);
    ParkingSlotEntity parkingSlotEntity = getParkingSlotDao().find(parkingSlot.getSlotNumber());

    parkingSlotEntity.setParkingLot(getParkingLotDao().find(parkingSlot.getParkingLotId()));
    ParkingSlotStatusEntity status = this.parkingSlotStatusDao.findOne(parkingSlot.getParkingSlotStatusId());
    parkingSlotEntity.setStatus(status);

    // initialize, validate parkingSlotEntity here if necessary
    ParkingSlotEntity resultEntity = getParkingSlotDao().save(parkingSlotEntity);
    LOG.debug("ParkingSlot with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, ParkingSlotEto.class);
  }

  /**
   * Returns the field 'parkingSlotDao'.
   *
   * @return the {@link ParkingSlotDao} instance.
   */
  public ParkingSlotDao getParkingSlotDao() {

    return this.parkingSlotDao;
  }

  @Override
  public ParkingSlotCto findParkingSlotCto(Long id) {

    LOG.debug("Get ParkingSlotCto with id {} from database.", id);
    ParkingSlotEntity entity = getParkingSlotDao().findOne(id);
    ParkingSlotCto cto = new ParkingSlotCto();
    cto.setParkingSlot(getBeanMapper().map(entity, ParkingSlotEto.class));
    cto.setParkingLotDetails(getBeanMapper().map(entity.getParkingLot(), ParkingLotEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<ParkingSlotCto> findParkingSlotCtos(ParkingSlotSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ParkingSlotEntity> parkingslots = getParkingSlotDao().findParkingSlots(criteria);
    List<ParkingSlotCto> ctos = new ArrayList<>();
    for (ParkingSlotEntity entity : parkingslots.getResult()) {
      ParkingSlotCto cto = new ParkingSlotCto();
      cto.setParkingSlot(getBeanMapper().map(entity, ParkingSlotEto.class));
      cto.setParkingLotDetails(getBeanMapper().map(entity.getParkingLot(), ParkingLotEto.class));
      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<ParkingSlotCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

  @Override
  public ReservationEto findReservation(Long id) {

    LOG.debug("Get Reservation with id {} from database.", id);
    return getBeanMapper().map(getReservationDao().findOne(id), ReservationEto.class);
  }

  @Override
  public PaginatedListTo<ReservationEto> findReservationEtos(ReservationSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ReservationEntity> reservations = getReservationDao().findReservations(criteria);
    return mapPaginatedEntityList(reservations, ReservationEto.class);
  }

  @Override
  public boolean deleteReservation(Long reservationId) {

    ReservationEntity reservation = getReservationDao().find(reservationId);
    getReservationDao().delete(reservation);
    LOG.debug("The reservation with id '{}' has been deleted.", reservationId);
    return true;
  }

  @Override
  public ReservationEto saveReservation(ReservationEto reservation) {

    Objects.requireNonNull(reservation, "reservation");

    // create and save slot
    ParkingSlotEto parkingSlotEto = new ParkingSlotEto();
    parkingSlotEto.setParkingLotId(reservation.getParkingLotId());
    parkingSlotEto.setSlotNumber(reservation.getParkingSlotId());
    parkingSlotEto.setParkingSlotStatusId(2L);
    parkingSlotEto = saveParkingSlot(parkingSlotEto);
    LOG.info("ParkingSlotEto  Obj  '{}' created.", parkingSlotEto);

    // update lot
    ParkingLotEto parkingLotEto = findParkingLot(reservation.getParkingLotId());
    parkingLotEto.setAvailableNoOfSlots(parkingLotEto.getAvailableNoOfSlots() - 1);
    parkingLotEto = saveParkingLot(parkingLotEto);
    LOG.info("ParkingLotEto  Obj  '{}' created.", parkingLotEto);

    // create and save reservation
    // reservation.setParkingLotDetailsId(parkingLotEto.getId());
    // reservation.setParkingSlotDetailsId(parkingSlotEto.getId());

    ReservationEntity reservationEntity = getBeanMapper().map(reservation, ReservationEntity.class);
    reservationEntity.setParkingLot(getParkingLotDao().findOne(parkingLotEto.getId()));
    reservationEntity.setParkingSlot(getParkingSlotDao().findOne(parkingSlotEto.getId()));
    reservationEntity.setItineraryId(reservation.getItineraryId());

    ReservationEntity resultEntity = getReservationDao().save(reservationEntity);

    ReservationEto resultEto = getBeanMapper().map(resultEntity, ReservationEto.class);

    LOG.info("ReservationEto  Obj  '{}' to be created.", resultEto);

    return resultEto;
  }

  /**
   * Returns the field 'reservationDao'.
   *
   * @return the {@link ReservationDao} instance.
   */
  public ReservationDao getReservationDao() {

    return this.reservationDao;
  }

  @Override
  public ReservationCto findReservationCto(Long id) {

    LOG.debug("Get ReservationCto with id {} from database.", id);
    ReservationEntity entity = getReservationDao().findOne(id);
    ReservationCto cto = new ReservationCto();
    cto.setReservation(getBeanMapper().map(entity, ReservationEto.class));
    cto.setParkingLotDetails(getBeanMapper().map(entity.getParkingLot(), ParkingLotEto.class));
    cto.setParkingSlotDetails(getBeanMapper().map(entity.getParkingSlot(), ParkingSlotEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<ReservationCto> findReservationCtos(ReservationSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ReservationEntity> reservations = getReservationDao().findReservations(criteria);
    List<ReservationCto> ctos = new ArrayList<>();
    for (ReservationEntity entity : reservations.getResult()) {
      ReservationCto cto = new ReservationCto();
      cto.setReservation(getBeanMapper().map(entity, ReservationEto.class));
      cto.setParkingLotDetails(getBeanMapper().map(entity.getParkingLot(), ParkingLotEto.class));
      cto.setParkingSlotDetails(getBeanMapper().map(entity.getParkingSlot(), ParkingSlotEto.class));

      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<ReservationCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

  @Override
  public ParkingSlotStatusEto findParkingSlotStatus(Long id) {

    LOG.debug("Get ParkingSlotStatus with id {} from database.", id);
    return getBeanMapper().map(getParkingSlotStatusDao().findOne(id), ParkingSlotStatusEto.class);
  }

  @Override
  public PaginatedListTo<ParkingSlotStatusEto> findParkingSlotStatusEtos(ParkingSlotStatusSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ParkingSlotStatusEntity> parkingslotstatuss = getParkingSlotStatusDao()
        .findParkingSlotStatuss(criteria);
    return mapPaginatedEntityList(parkingslotstatuss, ParkingSlotStatusEto.class);
  }

  @Override
  public boolean deleteParkingSlotStatus(Long parkingSlotStatusId) {

    ParkingSlotStatusEntity parkingSlotStatus = getParkingSlotStatusDao().find(parkingSlotStatusId);
    getParkingSlotStatusDao().delete(parkingSlotStatus);
    LOG.debug("The parkingSlotStatus with id '{}' has been deleted.", parkingSlotStatusId);
    return true;
  }

  @Override
  public ParkingSlotStatusEto saveParkingSlotStatus(ParkingSlotStatusEto parkingSlotStatus) {

    Objects.requireNonNull(parkingSlotStatus, "parkingSlotStatus");
    ParkingSlotStatusEntity parkingSlotStatusEntity = getBeanMapper().map(parkingSlotStatus,
        ParkingSlotStatusEntity.class);

    // initialize, validate parkingSlotStatusEntity here if necessary
    ParkingSlotStatusEntity resultEntity = getParkingSlotStatusDao().save(parkingSlotStatusEntity);
    LOG.debug("ParkingSlotStatus with id '{}' has been created.", resultEntity.getId());

    return getBeanMapper().map(resultEntity, ParkingSlotStatusEto.class);
  }

  /**
   * Returns the field 'parkingSlotStatusDao'.
   *
   * @return the {@link ParkingSlotStatusDao} instance.
   */
  public ParkingSlotStatusDao getParkingSlotStatusDao() {

    return this.parkingSlotStatusDao;
  }

  @Override
  public ParkingSlotStatusCto findParkingSlotStatusCto(Long id) {

    LOG.debug("Get ParkingSlotStatusCto with id {} from database.", id);
    ParkingSlotStatusEntity entity = getParkingSlotStatusDao().findOne(id);
    ParkingSlotStatusCto cto = new ParkingSlotStatusCto();
    cto.setParkingSlotStatus(getBeanMapper().map(entity, ParkingSlotStatusEto.class));

    return cto;
  }

  @Override
  public PaginatedListTo<ParkingSlotStatusCto> findParkingSlotStatusCtos(ParkingSlotStatusSearchCriteriaTo criteria) {

    criteria.limitMaximumPageSize(MAXIMUM_HIT_LIMIT);
    PaginatedListTo<ParkingSlotStatusEntity> parkingslotstatuss = getParkingSlotStatusDao()
        .findParkingSlotStatuss(criteria);
    List<ParkingSlotStatusCto> ctos = new ArrayList<>();
    for (ParkingSlotStatusEntity entity : parkingslotstatuss.getResult()) {
      ParkingSlotStatusCto cto = new ParkingSlotStatusCto();
      cto.setParkingSlotStatus(getBeanMapper().map(entity, ParkingSlotStatusEto.class));
      ctos.add(cto);

    }
    PaginationResultTo pagResultTo = new PaginationResultTo(criteria.getPagination(), (long) ctos.size());
    PaginatedListTo<ParkingSlotStatusCto> pagListTo = new PaginatedListTo(ctos, pagResultTo);
    return pagListTo;
  }

}
