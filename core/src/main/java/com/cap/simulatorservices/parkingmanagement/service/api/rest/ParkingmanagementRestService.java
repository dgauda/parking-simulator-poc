package com.cap.simulatorservices.parkingmanagement.service.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cap.simulatorservices.parkingmanagement.logic.api.Parkingmanagement;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingLotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ParkingSlotStatusSearchCriteriaTo;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationCto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationEto;
import com.cap.simulatorservices.parkingmanagement.logic.api.to.ReservationSearchCriteriaTo;

import io.oasp.module.jpa.common.api.to.PaginatedListTo;

/**
 * The service interface for REST calls in order to execute the logic of component {@link Parkingmanagement}.
 */
@Path("/parkingmanagement/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ParkingmanagementRestService {

  /**
   * Delegates to {@link Parkingmanagement#findParkingLot}.
   *
   * @param id the ID of the {@link ParkingLotEto}
   * @return the {@link ParkingLotEto}
   */
  @GET
  @Path("/parkinglot/{id}/")
  public ParkingLotEto getParkingLot(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#saveParkingLot}.
   *
   * @param parkinglot the {@link ParkingLotEto} to be saved
   * @return the recently created {@link ParkingLotEto}
   */
  @POST
  @Path("/parkinglot/")
  public ParkingLotEto saveParkingLot(ParkingLotEto parkinglot);

  /**
   * Delegates to {@link Parkingmanagement#deleteParkingLot}.
   *
   * @param id ID of the {@link ParkingLotEto} to be deleted
   */
  @DELETE
  @Path("/parkinglot/{id}/")
  public void deleteParkingLot(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#findParkingLotEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding parkinglots.
   * @return the {@link PaginatedListTo list} of matching {@link ParkingLotEto}s.
   */
  @Path("/parkinglot/search")
  @POST
  public PaginatedListTo<ParkingLotEto> findParkingLotsByPost(ParkingLotSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Parkingmanagement#findParkingLotCto}.
   *
   * @param id the ID of the {@link ParkingLotCto}
   * @return the {@link ParkingLotCto}
   */
  @GET
  @Path("/parkinglot/cto/{id}/")
  public ParkingLotCto getParkingLotCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#findParkingLotCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding parkinglots.
   * @return the {@link PaginatedListTo list} of matching {@link ParkingLotCto}s.
   */
  @Path("/parkinglot/cto/search")
  @POST
  public PaginatedListTo<ParkingLotCto> findParkingLotCtosByPost(ParkingLotSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Parkingmanagement#findParkingSlot}.
   *
   * @param id the ID of the {@link ParkingSlotEto}
   * @return the {@link ParkingSlotEto}
   */
  @GET
  @Path("/parkingslot/{id}/")
  public ParkingSlotEto getParkingSlot(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#saveParkingSlot}.
   *
   * @param parkingslot the {@link ParkingSlotEto} to be saved
   * @return the recently created {@link ParkingSlotEto}
   */
  @POST
  @Path("/parkingslot/")
  public ParkingSlotEto saveParkingSlot(ParkingSlotEto parkingslot);

  /**
   * Delegates to {@link Parkingmanagement#deleteParkingSlot}.
   *
   * @param id ID of the {@link ParkingSlotEto} to be deleted
   */
  @DELETE
  @Path("/parkingslot/{id}/")
  public void deleteParkingSlot(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#findParkingSlotEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding parkingslots.
   * @return the {@link PaginatedListTo list} of matching {@link ParkingSlotEto}s.
   */
  @Path("/parkingslot/search")
  @POST
  public PaginatedListTo<ParkingSlotEto> findParkingSlotsByPost(ParkingSlotSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Parkingmanagement#findParkingSlotCto}.
   *
   * @param id the ID of the {@link ParkingSlotCto}
   * @return the {@link ParkingSlotCto}
   */
  @GET
  @Path("/parkingslot/cto/{id}/")
  public ParkingSlotCto getParkingSlotCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#findParkingSlotCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding parkingslots.
   * @return the {@link PaginatedListTo list} of matching {@link ParkingSlotCto}s.
   */
  @Path("/parkingslot/cto/search")
  @POST
  public PaginatedListTo<ParkingSlotCto> findParkingSlotCtosByPost(ParkingSlotSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Parkingmanagement#findReservation}.
   *
   * @param id the ID of the {@link ReservationEto}
   * @return the {@link ReservationEto}
   */
  @GET
  @Path("/reservation/{id}/")
  public ReservationEto getReservation(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#saveReservation}.
   *
   * @param reservation the {@link ReservationEto} to be saved
   * @return the recently created {@link ReservationEto}
   */
  @POST
  @Path("/reservation/")
  public ReservationEto saveReservation(ReservationEto reservation);

  /**
   * Delegates to {@link Parkingmanagement#deleteReservation}.
   *
   * @param id ID of the {@link ReservationEto} to be deleted
   */
  @DELETE
  @Path("/reservation/{id}/")
  public void deleteReservation(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#findReservationEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding reservations.
   * @return the {@link PaginatedListTo list} of matching {@link ReservationEto}s.
   */
  @Path("/reservation/search")
  @POST
  public PaginatedListTo<ReservationEto> findReservationsByPost(ReservationSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Parkingmanagement#findReservationCto}.
   *
   * @param id the ID of the {@link ReservationCto}
   * @return the {@link ReservationCto}
   */
  @GET
  @Path("/reservation/cto/{id}/")
  public ReservationCto getReservationCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#findReservationCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding reservations.
   * @return the {@link PaginatedListTo list} of matching {@link ReservationCto}s.
   */
  @Path("/reservation/cto/search")
  @POST
  public PaginatedListTo<ReservationCto> findReservationCtosByPost(ReservationSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Parkingmanagement#findParkingSlotStatus}.
   *
   * @param id the ID of the {@link ParkingSlotStatusEto}
   * @return the {@link ParkingSlotStatusEto}
   */
  @GET
  @Path("/parkingslotstatus/{id}/")
  public ParkingSlotStatusEto getParkingSlotStatus(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#saveParkingSlotStatus}.
   *
   * @param parkingslotstatus the {@link ParkingSlotStatusEto} to be saved
   * @return the recently created {@link ParkingSlotStatusEto}
   */
  @POST
  @Path("/parkingslotstatus/")
  public ParkingSlotStatusEto saveParkingSlotStatus(ParkingSlotStatusEto parkingslotstatus);

  /**
   * Delegates to {@link Parkingmanagement#deleteParkingSlotStatus}.
   *
   * @param id ID of the {@link ParkingSlotStatusEto} to be deleted
   */
  @DELETE
  @Path("/parkingslotstatus/{id}/")
  public void deleteParkingSlotStatus(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#findParkingSlotStatusEtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding parkingslotstatuss.
   * @return the {@link PaginatedListTo list} of matching {@link ParkingSlotStatusEto}s.
   */
  @Path("/parkingslotstatus/search")
  @POST
  public PaginatedListTo<ParkingSlotStatusEto> findParkingSlotStatussByPost(
      ParkingSlotStatusSearchCriteriaTo searchCriteriaTo);

  /**
   * Delegates to {@link Parkingmanagement#findParkingSlotStatusCto}.
   *
   * @param id the ID of the {@link ParkingSlotStatusCto}
   * @return the {@link ParkingSlotStatusCto}
   */
  @GET
  @Path("/parkingslotstatus/cto/{id}/")
  public ParkingSlotStatusCto getParkingSlotStatusCto(@PathParam("id") long id);

  /**
   * Delegates to {@link Parkingmanagement#findParkingSlotStatusCtos}.
   *
   * @param searchCriteriaTo the pagination and search criteria to be used for finding parkingslotstatuss.
   * @return the {@link PaginatedListTo list} of matching {@link ParkingSlotStatusCto}s.
   */
  @Path("/parkingslotstatus/cto/search")
  @POST
  public PaginatedListTo<ParkingSlotStatusCto> findParkingSlotStatusCtosByPost(
      ParkingSlotStatusSearchCriteriaTo searchCriteriaTo);

}
